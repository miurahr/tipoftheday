/*
 *
 * Copyright 2004 Sun Microsystems, Inc.
 *           2023 Hiroshi Miura
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
package tokyo.northside.tipoftheday.tips;

import tokyo.northside.tipoftheday.TipOfTheDay;

/**
 * A model for {@link TipOfTheDay}.
 *
 * @author Frederic Lavigne
 * @author Hiroshi Miura
 */
public interface TipOfTheDayModel {

    /**
     * @return the number of tips in this model
     */
    int getTipCount();

    /**
     * @param index a tip text index to show.
     * @return the tip at <code>index</code>
     * @throws IndexOutOfBoundsException
     *           if the index is out of range (index &lt; 0 || index &gt;=
     *           getTipCount()).
     */
    Tip getTipAt(int index);
}
