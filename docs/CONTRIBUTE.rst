==================
Contribution guide
==================

This is contribution guide for tipoftheday project.
You are welcome to send a Pull-Request, reporting bugs and ask questions.

Resources
=========

- Project owner: Hiroshi Miura
- Bug Tracker:  CodeBerg issue `Tracker`_
- Status: Alpha
- Activity: moderate

.. _`Tracker`: https://codeberg.org/miurahr/tipoftheday/issues

Bug triage
==========

Every report to github issue tracker should be in triage.
whether it is bug, question or invalid.


Send patch
==========

Here is small amount rule when you want to send patch the project;

1. every proposal for modification should send as 'Pull Request'

1. each pull request can consist of multiple commits.

1. you are encourage to split modifications to individual commits that are logical subpart.
