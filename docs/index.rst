=============================================
Java/Swing "Tip of the Day" component library
=============================================

Development status
==================

A status of library development is considered as `Beta`.

License
=======

It is distributed under GNU Lesser General Public License version 2.1 or (at your option) any later version.
Please see LICENSE file in project root.

Contents
========

.. toctree::
   :maxdepth: 1

   HowToUse
   SECURITY
   CONTRIBUTE
   CODE_OF_CONDUCT
   CHANGELOG

Index
=====

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
